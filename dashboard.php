<?php 
session_start();
require_once('header.php');
require_once('config.php');
if(isset($_REQUEST['del']))
{
 if(isset($_SESSION["id"])){   
    $uid=intval($_GET['del']);
    $sql = "DELETE from category WHERE  id=:id";
    $stmt = $conn->prepare($sql);
    $stmt-> bindParam(':id',$uid, PDO::PARAM_STR);
    $stmt -> execute();
    echo "<script>window.location.href='dashboard.php'</script>"; 
    }  
    else {
        header("location: index.php"); 
    }
}
?>
<body style=" margin-top:70px; background-color: #F2F2F2 ">
 <div class="container-fluid" >	
  <div class="row">
    <div class="col-12">
      <div class="card">
       <div class="card-body">
       <h3 class="card-title py-2 text-center">Manage Category</h3>
         <form method="post" "> 
        <div class="form-body">
         <div class="row mx-5">
            <div class="col-md-3 col-10">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" name="cat">
                </div>
            </div>
            <div class="col-md-2 col-2">
                <div class="form-group">
                    <button type="submit" name="search" class="btn btn-primary">Search</button>
                </div>
            </div>
            <div class="col-md-3 col-10">
                <div class="form-group">								
                    <select class="form-control" name="cat">
                        <option value="" disabled selected>Choose Category</option>
                                                                <option value="Samsung">Samsung</option>
                                 <option value="Realmi">Realmi</option><option value="Iphone">Iphone</option>								
                    </select>
                </div>
            </div>
            
            <div class="col-md-2 col-2">
                <div class="form-group">
                    <button type="submit"  name="filter" class="btn btn-primary">Filter</button>
                </div>
            </div>
            <div class="col-md-1 col-3">
            <div class="form-group">    
            <input type="submit" name="sort" value="Sort" class="btn btn-secondary"><br>
            </div>
        </div>
        <div class="col-md-1 col-3">
            <div class="form-group">    
            <input type="submit" name="s" value="Reset" class="btn btn-secondary"><br>
            </div>
            </div>
            </div>
        </div>
    </form>
    
        <div class="table-responsive col-md-12 ">
        <table class="table table-bordered no-wrap text-center">
            <thead>
        <tr>
            <th width=5%>Sl.No.</th>
            <th width=20%> Name</th>
            <th width=20%>Image</th>
            <th width=20% >Status</th>
            <th width=20%>Options</th>
        </tr>
    </thead>
    <tbody id="filter_result">
<?php 

   if(isset($_POST['sort']))
     {
     $sql = "SELECT name,image, status
     from category ORDER BY name ASC ";
     }
   else if(isset($_POST['search']) && !empty($_POST['cat'])) {
      $n=$_POST['cat'];
     $sql = " SELECT * FROM `category` WHERE name LIKE '%$n%' ";
     }
    else if(isset($_POST['filter']) && !empty($_POST['cat'])) {
      $n=$_POST['cat'];
     $sql = " SELECT * FROM `category` WHERE name = '$n' ";
     }
    else{
     $sql = "SELECT * from category";
     }                  

     $stmt = $conn->prepare($sql);
     $stmt->execute();
     $results=$stmt->fetchAll(PDO::FETCH_OBJ);  
     $array = json_decode(json_encode($results), true);
     $cnt=1;
    if($stmt->rowCount() > 0)
     {
     foreach($results as $result)
     {               
     ?>  
     <tr>
     <td><?php echo htmlentities($cnt);?></td>
     <td><?php echo htmlentities($result->name);?></td>
     <td><img src="<?php echo htmlentities($result->image);?>"width="70" height="70"></td>
     <td><?php echo htmlentities($result->status);?></td>

     <td><a href="edit.php?id=<?php echo $result->id;?>"><button class="btn btn-success btn-xs mx-2">Edit</button></a>
     <a href="dashboard.php?del=<?php echo htmlentities($result->id);?>"><button class="btn btn-danger btn-xs mx-2" onClick="return confirm('Do you really want to delete');"><span class="glyphicon glyphicon-trash"></span>Delete</button></a></td>
     </tr>
     <?php 
     $cnt++;
      }} 
     ?>
       </tbody>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>

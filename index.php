

<?php
session_start();
require_once ('header.php');
require_once ("config.php");

$message="";
if(count($_POST)>0) {
 
    if(empty($_POST["name"])){
    $name_error="Name is required";
    }
    else if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["name"])){
     $name_error="Enter a valid name";
    }
    else{
    //$name =check($_POST["name"]);
    $name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    }   

$sql ="SELECT * FROM admin WHERE (name=:usname) and (password=:usrpass)";
    $query= $conn -> prepare($sql);
    $query-> bindParam(':usname', $name, PDO::PARAM_STR);
    $query-> bindParam(':usrpass', $_POST["pass"], PDO::PARAM_STR);
    $query-> execute();
    $result=$query->fetch(PDO::FETCH_OBJ);
    $array = json_decode(json_encode($result), true);
    //print_r($array);
   if($query->rowCount() > 0)
  {
    $_SESSION['name']=$name;
    $_SESSION["id"] = $array['id'];
  }
 else {
$message = "Invalid Username or Password!";
}
}
if(isset($_SESSION["name"])) {
header("Location:dashboard.php");
}
?>

<body style="margin-top:90px; background-color: #F2F2F2 ;  ">
<div class="  row  col-md-12 mb-5 " style=" justify-content: center;">
    <div class=" col-md-5 col-10 py-2 mx-3 mt-3" style=" background-color: #ffffff  ;  
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); " >  

<form name="name" method="post" action="" id="login"  >   
<h4 class=" text-center">Please Login Here</h4>
<div class=" text-center"  > </div> <br>
<div class="text-center" >
 Username:<br>
 <input type="text" name="name" class="wid" placeholder="Username"> 
 <br><br>
 Password:<br>
<input type="password" name="pass" class="wid" placeholder="Password"></div> 
<div class="text-center mt-2 py-4">
<input type="submit" name="login" value="Login" class="btn btn-success">
<input type="reset"class="btn btn-secondary" ></div>
</form>
 </div>
<div class=" col-md-4 col-10 py-5 text-center mx-3 mt-3" style=" background-color: #ffffff ;
 box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); ">
<h4 class="monospace" style="padding-top: 30px;">Not Registered yet,<br> Sign Up here</h4><br>
<p><a href="register.php"class="btn btn-primary">Sign Up</a></p>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
            $('#login').validate({
                rules: {
                    name: {
                        required : true,
                    },
                  
                    pass: {
                        required : true,
                    } 
                },
                messages: {
                    name: {
                        required : "Please Enter a valid Username",
                    },
                    pass: {
                        required : "Please Enter a valid Password",
                    }
                }
            });
        });
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>       
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>    
</body>
</html>
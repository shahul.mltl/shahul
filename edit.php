<?php
session_start();
require_once 'config.php';
require_once 'header.php';
$cname =$image=$status=$name_error=
$image_error=$status_error= "";
if(isset($_SESSION["id"])){  
if(isset($_POST["submit"]))
{  
$cid=$_POST["id"];  
if(empty($_POST["name"])){
    $name_error="Name is required";
    }
    else if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["name"])){
        $name_error="Enter a valid name";
    }
    else{
    $cname = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    }

   if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["status"]) ){
        $status_error="Enter a valid status";
    }
    else{
    $status = filter_var($_POST["status"], FILTER_SANITIZE_STRING);

    }
    date_default_timezone_set("Asia/Calcutta");
    $uploads_dir = 'uploads';
    $count = 1;
    foreach ($_FILES["image"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["image"]["tmp_name"][$key];
            $name = $_FILES["image"]["name"][$key];
            $uploadfile = "$uploads_dir/$name";
            $ext = strtolower(substr($uploadfile,strlen($uploadfile)-3,3));
            if (preg_match("/(jpg|jpeg|png)/",$ext)){
                $newfile = "$uploads_dir/img_".date('d-m-Y_his').str_pad($count++,2,'0',STR_PAD_LEFT).".".$ext;
                $image = filter_var($newfile, FILTER_SANITIZE_STRING);
                move_uploaded_file($tmp_name, $newfile);
                }
        }
    }
if(isset($_POST["submit"]) && !empty($name) && !empty($image) && !empty($status)){
  $sqli="UPDATE category SET name=:fn,image=:im,status=:st where id=:uid";
  $stmt = $conn->prepare($sqli);
  $stmt->bindParam(':fn',$cname,PDO::PARAM_STR);
  $stmt->bindParam(':im',$image,PDO::PARAM_STR);
  $stmt->bindParam(':st',$status,PDO::PARAM_STR);
  $stmt->bindParam(':uid',$cid,PDO::PARAM_STR);
// Query Execution
if($stmt->execute()){
  echo "<script>window.location.href='dashboard.php'</script>";
} 
}
$conn = null;
}
}
else{
    header("location: index.php");  
}
?>
 <body style=" margin-top:70px; background-color: #F2F2F2 ">
 <div class="col-md-12 text-center " style="justify-content: center;" >
        <div class="container-fluid col-md-4 col-sm-6 col-8">
            <div class="row">
                <div>
                    <div class="page-header">
                        <h2>Edit Category</h2>
                    </div>
                    <p>Please Edit values and submit the record.</p>

<?php 
if(isset($_GET['id'])){
    $id =intval($_GET['id']);

    $sql = "SELECT * from category where id=:uid";
    $query = $conn->prepare($sql);
    $query->bindParam(':uid',$id,PDO::PARAM_STR);
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_OBJ);
    $cnt=1;
    if($query->rowCount() > 0)
    {
    foreach($results as $result)
{               
?>
<form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
    Name<br> <input type="text" name="name" value="<?php echo htmlentities($result->name);?>" class="form-control">
    <br><span><?php echo $name_error;?></span><br>
    Image<br> <input type="file" name="image[]" value="<?php echo $tmp_name;?>" class="form-control" ><br><br>
    Status<br>
    <select  name="status" class="form-control" >
    <option ><?php echo $result->status;?></option>
        <option value='<?php if($result->status=="Disable"){echo "Enable";} else{ echo "Disable";}?>'>
        <?php if($result->status=="Disable"){echo "Enable";} else{ echo "Disable";}?></option> 								
    </select><br>
    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
    <?php }}
    }
    ?>
    
    <br><input type="submit" name="submit" value="Update" class="btn btn-primary">
    <a href="dashboard.php" class="btn btn-secondary">Cancel</a>
</form>  
	</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#add').validate({
        rules: {
            name: {
                required : true,
                minlength: 3,
                maxlength: 50
            },
            image: {
                required : true,
                extension: "jep | jpeg | png",
                filesize : 500
            },
            status: {
                required : true,
            }
        },
    // set validation messages for the rules are set previously
        messages: {
            name: {
                required : "Enter a valid Username",
                minlength: "Username must contain at least 3 characters",
                minlength: "Username must contain at least 50 characters"
            },
            image: {
                required : "Select a valid Image ",
                minlength: "Username must contain at least 8 characters",
                extension: "jpg/jpeg/png only",
                filesize : "maximum 500kb"
                
            },
            status: {
                required : "Choose a valid Status",
            }
        }
    });
});
</script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>   
</body>
</html>


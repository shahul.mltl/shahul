<?php
session_start();
require_once "header.php";
require_once "config.php";

$cname =$image=$status=$name_error=
$image_error=$status_error= "";
 
if(isset($_SESSION["id"])){
    
    function check($x)
    {
    $x=trim($x);
    $x=stripslashes($x);
    return $x;
    }  
  if($_SERVER["REQUEST_METHOD"]=="POST")
  {
       
  if(empty($_POST["name"])){
    $name_error="Name is required";
    }
    else if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["name"])){
        $name_error="Enter a valid name";
    }
    else{
    $cname = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    }

   if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["status"]) ){
        $status_error="Enter a valid status";
    }
    else{
    $status = filter_var($_POST["status"], FILTER_SANITIZE_STRING);

    }
    
    date_default_timezone_set("Asia/Calcutta");
    $uploads_dir = 'uploads';
    $count = 1;
    foreach ($_FILES["image"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES["image"]["tmp_name"][$key];
            $name = $_FILES["image"]["name"][$key];
            $uploadfile = "$uploads_dir/$name";
            $ext = strtolower(substr($uploadfile,strlen($uploadfile)-3,3));
            if (preg_match("/(jpg|jpeg|png)/",$ext)){
                $newfile = "$uploads_dir/img_".date('d-m-Y_his').str_pad($count++,2,'0',STR_PAD_LEFT).".".$ext;
                $image = filter_var($newfile, FILTER_SANITIZE_STRING);
                move_uploaded_file($tmp_name, $newfile);
                }
            }
            }
  
     if(isset($_POST["submit"]) && !empty($name) && !empty($image) && !empty($status)){
            $stmt = $conn->prepare("INSERT INTO category (name, image, status)
            VALUES (:name, :image, :status)");
            
            $stmt->bindParam(':name', $cname);
            $stmt->bindParam(':image', $image);
            $stmt->bindParam(':status', $status);      
            if($stmt->execute()){
                header("location: dashboard.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
    }
    
    $conn = null;
}
else{
    header("location: index.php");  
}
?>
 
 <body style=" margin-top:70px; background-color: #F2F2F2 ">
    <div class="col-md-12 text-center " >
      <div class="container-fluid col-md-4 col-sm-6 col-8">
        <div class="row">
          <div class="card d-block h-100 box-shadow-hover pointer">
           <div class="pt-3 h-75p align-items-center d-flex justify-content-center">
           <h2>Add Category</h2>
          </div>
         <div class="card-body p-4">
            <p>Please input values and submit .</p>   
            <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" 
            method="POST" id="add">
                    Name<br> <input type="text" name="name" placeholder="Category Name" class="form-control">
                <span><?php echo $name_error;?></span><br>
                Image<br> <input type="file" name="image[]" class="form-control"><br>
                Status<br>
                <select  name="status" class="form-control">
                <option >Choose Category</option>
                    <option value="Enable">Enable</option>
                    <option value="Disable">Disable</option>								
                </select>
                <br><input type="submit" name="submit" value="submit" class="btn btn-primary">
                <a href="dashboard.php" class="btn btn-secondary">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>
    <script type="text/javascript">
$(document).ready(function(){
    $('#add').validate({
        rules: {
            name: {
                required : true,
                minlength: 3,
                maxlength: 50
            },
            image: {
                required : true,
                extension: "jep | jpeg | png",
                filesize : 500
            },
            status: {
                required : true,
            }
        },
    // set validation messages for the rules are set previously
        messages: {
            name: {
                required : "Enter a valid Username",
                minlength: "Username must contain at least 3 characters",
                maxlength: "Username must contain at least 50 characters"
            },
            image: {
                required : "Select a valid Image ",
                minlength: "Username must contain at least 8 characters",
                extension: "jpg/jpeg/png only",
                filesize : "maximum 500kb"
                
            },
            status: {
                required : "Choose a valid Status",
            }
        }
    });
});
</script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>    
</body>
</html>
<?php require_once('header.php');
?>
<?php
require_once "config.php";
$name =$email=$pass= "";

function check($x)
{
$x=trim($x);
$x=stripslashes($x);
return $x;
}

if($_SERVER["REQUEST_METHOD"]=="POST")
{
if(empty($_POST["name"])){
$name_error="Name is required";
}
else if(!preg_match("/^[a-zA-Z-' ]*$/",$_POST["name"])){
 $name_error="Enter a valid name";
}
else{
//$name =check($_POST["name"]);
$name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
}
if(empty($_POST["email"])){
    $email_error="Email is required";
    }
else if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
     $email_error = "Invalid email format";
    }
else {
    $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    //$email = check($_POST["email"]);
}
$address = filter_var($_POST["address"], FILTER_SANITIZE_STRING);
$state = filter_var($_POST["state"], FILTER_SANITIZE_STRING);
$country = filter_var($_POST["country"], FILTER_SANITIZE_STRING);
$phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
if(empty($_POST["pass"])){
    $pass_error="Password is required";
    }
    
   else{
   $pass =check($_POST["pass"]);
   }
}

if(!empty($name) && !empty($email) && !empty($pass)){

        $stmt = $conn->prepare("INSERT INTO admin (name, email, address, state, country, phone, password)
        VALUES (:name, :email,:address, :state, :country, :phone, :pass )");
        
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':address', $address);
        $stmt->bindParam(':state', $state);
        $stmt->bindParam(':country', $country);
        $stmt->bindParam(':phone', $phone);
        $stmt->bindParam(':pass', $pass);

       if( $stmt-> execute()){

        header("location:index.php");
       }
    }
  
?>
<body style=" margin-top:70px; background-color: #F2F2F2 ">
<div class="row col-md-12 text-center" style="justify-content: center;">

 <div class="container-fluid col-md-4 col-sm-6 col-10">  
<h3 >Register here</h3><br> 

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" id="signup" method="POST">
   <div class="form-group">
    <input type="text" class="form-control" id="namee" name="name" placeholder="Username" class="wid">
  </div> 
  <div class="form-group">
    <input type="email" class="form-control" name="email" placeholder="Email" class="wid">
  </div>
  <div class="form-group">
    <input type="textarea" class="form-control" name="address" placeholder="Adress" class="wid">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="state" placeholder="State" class="wid">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="country" placeholder="Country" class="wid">
  </div>
  <div class="form-group">
    <input type="text" class="form-control" name="phone" placeholder="Phone" class="wid">
  </div>
  <div class="form-group">
    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" class="wid">
  </div>
  <div class="form-group">
    <input type="password" class="form-control" id="cpassword" name="cpassword" class="wid"
 placeholder="Confirm Password">
  </div>
  <button type="submit" name="save" value="submit" class="btn btn-primary">Submit</button>
</form>
<h4 class="mt-4">Already Registered? Login here</h4><br>
<a href="index.php" class="btn btn-success">Login</a>
 </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
    
        $('#signup').validate({
            rules: {
                name: {
                    required : true,
                    minlength: 3,
                    maxlength: 50
                },
                email: {
                    required : true,
                    email: true
                },
                pass: {
                    required : true,
                    minlength: 8
                },
                cpassword: {
                    required : true,
                    equalTo: "#pass"
                }
            },
            messages: {
                name: {
                    required : "Enter a valid Username",
                    minlength: "Username must contain at least 3 characters",
                    maxlength: "Username must contain at least 50 characters"
                },
                email: {
                    required : "Enter a valid Email ",
                    email: "Enter a valid email. Ex: example@gmail.com"
                },
                pass: {
                    required : "Enter a valid Password",
                    minlength: "Password must contain at least 8 characters"
                },
                cpassword: {
                    required : "Enter a valid Password",
                    equalTo: "Password don't matching with the previous one."
                }
            }
        });
    });
    </script>
    <!-- Bootstrap JS file -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>     
    <!-- Validation JS file -->     
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>    
</body>
</html>
<!DOCTYPE html>
<html>
	<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">    
   <meta http-equiv="X-UA-Compatible" content="ie=edge"> 	
   
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">     
        <link  href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >	 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>           
<!-- For main header navbar -->

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark fixed-md-rowrap  shadow py-0" >
<a class="navbar-brand ml-3 " href="dashboard.php"><h3>Home</h3></a>

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    <span class="icon-bar"></span>
        <span class="icon-bar"></span>  
  </button>
  
  <div class=" collapse navbar-collapse " id="navbarsExampleDefault" >
    <ul class=" navbar-nav ml-auto mr-3 " >
      <li class="nav-item ">
    <a class="nav-link active" href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a> </li>
    <li class="nav-item ">
    <a class="nav-link active" href="add.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Add-Item</a> </li>
        <li class="nav-item">
        <a class="nav-link active" href="<?php echo ('logout.php'); ?>">
        <?php if (isset($_SESSION["name"]))
        { echo '<i class="fa fa-sign-out" aria-hidden="true"></i>';
            echo "Logout";
        } 
        else{ 
          echo '<i class="fa fa-sign-in" aria-hidden="true"></i>';
          echo "Login";}
        ?></a> </li>
        </ul>
  </div>   
</nav>
<style>
.monospace {
  font-family: "Lucida Console", Courier, monospace;
}
.wid {
  width: 60%;
}
table.center {
  margin-left:auto; 
  margin-right:auto;
  width: 100%;
}      
.alert {
   width:80%; 
   display:inline-block;
}

.error{
      color: red ;
  border-color: #ebccd1;

   }

</style>
</head>